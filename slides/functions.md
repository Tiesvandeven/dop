# Managing dependencies

* OO combines state with functionality
* DOP separates them

---slide---

## Implementing save()

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">class Person {

  fun save() : Unit = TODO()
	
}
</code>
</pre>

---slide---

## Implementing save()

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">class Person(dataAccessHelper : DataAccessHelper) {

  fun save() : Unit = dataAccessHelper.save(this)
	
}
</code>
</pre>

---slide---

## Implementing save()

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">class Person() {

  fun save() : Unit = DataAccessHelper.instance().save(this)
	
}
</code>
</pre>

---slide---

## Implementing save()

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">class Person() {

  fun save(dataAccessHelper : DataAccessHelper) : Unit = dataAccessHelper.save(this)
	
}
</code>
</pre>

---slide---

## Dependencies in OO

<img style="height: 700px;" src="slides/images/dependencies.svg">

Note:
@startuml
!theme hacker
class Component1 {
  usecase1()
}

class  Component2{
  usecase2()
}
class Person{
  usecase1()
  usecase2()
}


class RestHelper
class DataAccessHelper

Component1 --> Person
Component2 --> Person
Person --> RestHelper
Person --> DataAccessHelper
@enduml

---slide---

# Scaling usecases

---slide---

## Applications only grow


<img style="height: 500px;" src="slides/images/data_trap_2x.png">

---slide---

## So our OO classes grow

<img style="height: 500px;" src="slides/images/person1.svg">

Note:
@startuml
!theme hacker
class Person{
  usecase1()
  usecase2()
}

class Person^{
  usecase1()
  usecase2()
  usecase3()
  usecase4()
  usecase5()
  usecase6()
}
@enduml

---slide---

## The DOP way

* Seperate state and functionality
* Create a component for every usecase
* Pass the data as arguments

---slide---

## Putting logic in components

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">object Component1 {
    
  fun usecase1(person : Person) : Unit = dataAccessHelper.save(person)
    
}

object Component2 {
    
  fun usecase2(person : Person) : Unit = restHelper.save(person)
    
}
</code>
</pre

---slide---

## But this looks familiar...

---slide---

## Dependencies in DOP

<img style="height: 700px;" src="slides/images/components.svg">


Note:
@startuml
!theme hacker
class Component1 {
  usecase1()
}

class  Component2{
  usecase2()
}
class Person
class RestHelper
class DataAccessHelper

Component1 --> Person
Component2 --> Person
Component2 -up-> RestHelper
Component1 -up-> DataAccessHelper
@enduml

---slide---

## Should you combine state and functionality?

* Be careful with dependencies
* Be careful with businesslogic
* Be pragmatic not dogmatic

---slide---

## Why not both

<img style="height: 700px;" src="slides/images/both.jpg">

---slide---

## Extension functions

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">data class User(val name : String)

fun User.myUseCase() {

    println(this.name)
	
}


fun logic(){

    User("Bob").myUseCase()
	
}
</code>
</pre>

---slide---

# DOP and OO patterns

---slide---

## Polymorphism

* Sharing type
* Strategy pattern

---slide---

## Strategy in OO

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">interface CreditCard {
  fun pay() : Unit
}

class MasterCard : CreditCard {
  override fun pay() = TODO()
    
}

class VisaCard : CreditCard {
  override fun pay() = TODO()

}

fun doLogic(creditCard: CreditCard) {
  creditCard.pay()
}
</code>
</pre>

---slide---

## Strategy in DOP

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">interface CreditCard

class MasterCard : CreditCard

class VisaCard : CreditCard

fun doLogic(creditCard: CreditCard) {

  when(creditCard){
	
    is MasterCard -> MasterCardPayService.pay(creditCard)
		
    is VisaCard -> VisaCardPayService.pay(creditCard)
		
    else -> throw IllegalStateException("Unknown CC")
  }
}
</code>
</pre>

---slide---

## Introducing sealed classes

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%">sealed interface CreditCard

class MasterCard : CreditCard

class VisaCard : CreditCard

fun doLogic(creditCard: CreditCard) {

  when(creditCard){
	
    is MasterCard -> MasterCardPayService.pay(creditCard)
		
    is VisaCard -> VisaCardPayService.pay(creditCard)
	
  }
}
</code>
</pre>
