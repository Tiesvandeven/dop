# Data Oriented Programming

---slide---

# But first...

---slide---

<img style="height: 500px;" src="slides/images/duty_calls.png">

---slide---

>> "Don't be a Functional programmer, don't be an Object Oriented programmer, be a _better_ programmer"

- Brian Goetz

---slide---

# WHOAMI

<img style="float: right; height: 500px;" src="slides/images/ties.jpg">

* Ties van de Ven
* Java/Kotlin/Scala
* Functional Programming
* Software engineering principles

---slide---

<img style="height: 500px;" src="slides/images/dop.png">


https://www.infoq.com/articles/data-oriented-programming-java/

---slide---

# Agenda

* What is DOP
* Why should you use DOP
* How should you use DOP